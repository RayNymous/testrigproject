﻿using System;
using UnityEngine;
using System.Collections;

public class SoldierAnim : MonoBehaviour
{

    //animation and containers
    private Animator anim;

    //walk directions
    public float inputH { get; set; }

    public float inputV { get; set; }
  
//Animation Executed From States
#region actions

    public bool Run
    {
        get { return _runing; }
        set
        {
            //ClearAllActions();
            _prevRunning = _runing;
            _runing = value;
        }
    }
    private bool _runing = false;
    private bool _prevRunning = false;

    public bool Walk
    {
        get { return _walking; }
        set
        {
            ClearAllActions();
            _walking = value;
        }
    }
    private bool _walking = false;

    public bool Shoot
    {
        get { return _shooting; }
        set
        {
            ClearAllActions();
            _shooting = value;
        }
    }
    private bool _shooting = false;

    public void MakeReload()
    {
        ClearAllActions();
        _reloading = true;
    }
    private bool _reloading = false;

    public void ThrowGranate()
    {
        ClearAllActions();
        _granateThrowing = true;
    }
    private bool _granateThrowing = false;

    #endregion

//Global Animation States
#region States
    public bool Lay
    {
        get { return _laying; } 
        set
        {
            ClearAllStates();
            _laying = value;
        } 
    }
    private bool _laying = false;
    public bool Sit
    {
        get { return _siting; }
        set
        {
            ClearAllStates();
            _siting = value;
        }
    }
    private bool _siting = false;


    public bool Stand
    {
        get { return _standing; }
        set
        {
            ClearAllStates();
            _standing = value;
        }
    }
    private bool _standing = false;

#endregion


    public void ClearAllStates()
    {
        _runing= false;
        _laying = false;
        _siting = false;
        _shooting = false;
        _standing = false;
    }

    public void ClearAllActions()
    {
        _walking = false;
        _runing = false;
        _shooting = false;
        _reloading = false;
    }


    // Use this for initialization
	void Start ()
	{
	    anim = GetComponent<Animator>();
	}
	
	void Update () {


//	    anim.SetBool("lay", _laying);
//	    anim.SetBool("sit", _siting);
//        anim.SetBool("shoot", _shooting);
//        anim.SetBool("walk", _walking);
//        anim.SetBool("stand", _standing);

        if(_prevRunning!=_runing)
            anim.SetBool("run", _runing);


/*	    if (_reloading)
	    {
            anim.SetBool("reload",true);
            if(Lay)
	        {
	            Lay = false;
	            Sit = true;
	            _reloading = false;
	        }
	        _reloading = false;
	    }

        anim.SetFloat("inputH", inputH);
        anim.SetFloat("inputV", inputV);
        */
	}

}
