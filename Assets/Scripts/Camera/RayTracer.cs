﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RayTracer : MonoBehaviour
{
    public RectTransform ContextPanel;
    private ContextMenu ContextMenu;

    private float _timer;
    public float Timer = 0.5f;
    private bool _enableTrace = true;
    public GameObject Gizmo;
    public Vector3 HitPosition;

    public static RayTracer Instance { get; private set; }

  //  public bool SetActive
  //  {
   //     set { gameObject.SetActive(value); }
  //  }

    void Start()
    {
        Instance = this as RayTracer;
    }

    public RaycastHit GetRayHit()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100500))
            Debug.DrawLine(ray.origin, hit.point);

        Collider objectTouching = hit.collider;
        return hit;
    }

    void Update()
    {
        if (!_enableTrace)
        {
            _timer += Time.deltaTime;
            if (Timer <= _timer)
            {
                _enableTrace = true;
                _timer = 0.0f;
            }
            return;
        }

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit = GetRayHit();

            Vector2 viewPointPos = Camera.main.WorldToViewportPoint(hit.point);
            ContextMenu.Instance.SetActive = true;

            ContextPanel.anchorMin = viewPointPos;
            ContextPanel.anchorMax = viewPointPos;

            ContextMenu.Instance.CurrentHit = hit;
            ContextMenu.Instance.HitPosition = hit.point;
            _enableTrace = false;
           // gameObject.SetActive(false);
            enabled = false;
            if (hit.point == Vector3.zero)
                return;


            ContextMenu.Instance.Target = hit.transform.gameObject;
          //  ContextMenu.HitPosition = hit.point;


            
        }




    }
}
