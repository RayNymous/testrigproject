﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MayaStyleCameraNavigation : MonoBehaviour {

    public float zoomSpeed = 1.2f;
    public float moveSpeed = 1.9f;
    public float rotateSpeed = 4.0f;

    Material optionalMaterialForSelection;

    private GameObject orbitVector;
    private Material materialForSelection;
    private List<GameObject> selectedObjects = new List<GameObject>();
    private List<Material> selectedObjectsMaterial = new List<Material>();

    // Use this for initialization
    void Start() {
        orbitVector = GameObject.CreatePrimitive(PrimitiveType.Capsule);
        orbitVector.transform.position = Vector3.zero;
        transform.position = new Vector3(0, 30, 60);
        transform.LookAt(orbitVector.transform.position, Vector3.up);
        orbitVector.GetComponent<Renderer>().enabled = false;

        if (optionalMaterialForSelection)
        {
            materialForSelection = optionalMaterialForSelection;
        }
        else
        {
            materialForSelection = new Material(Shader.Find("Diffuse"));
            materialForSelection.color = Color.green;
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");


        //if alt pressed, start navigation

        if (Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.LeftAlt))
        {
            var distanceToOrbit = Vector3.Distance(transform.position, orbitVector.transform.position);

            //RMB - ZOOM
            if (Input.GetMouseButton(1))
            {
                // Refine the rotateSpeed based on distance to orbitVector
                var CurrentZoomSpeed = Mathf.Clamp(zoomSpeed * (distanceToOrbit / 50), 0.1f, 2.0f);

                // Move the camera in/out
                transform.Translate(Vector3.forward * (x * CurrentZoomSpeed));

                // If about to collide with the orbitVector, repulse the orbitVector slightly to keep it in front of us
                if (Vector3.Distance(transform.position, orbitVector.transform.position) < 3)
                {
                    orbitVector.transform.Translate(Vector3.forward, transform);
                }
            }
            //LMB - PIVOT

            else if (Input.GetMouseButton(0))
            {

                // Refine the rotateSpeed based on distance to orbitVector
                var currentRotateSpeed = Mathf.Clamp(rotateSpeed * (distanceToOrbit / 50), 1.0f, rotateSpeed);

                // Temporarily parent the camera to orbitVector and rotate orbitVector as desired
                transform.parent = orbitVector.transform;
                orbitVector.transform.Rotate(Vector3.right * (y * currentRotateSpeed));
                orbitVector.transform.Rotate(Vector3.up * (x * currentRotateSpeed), Space.World);
                transform.parent = null;
            }
            //MMB - PAN

            else if (Input.GetMouseButton(2))
            {

                // Calculate move speed
                var translateX = Vector3.right * (x * moveSpeed) * -1;
                var translateY = Vector3.up * (y * moveSpeed) * -1;

                // Move the camera
                transform.Translate(translateX);
                transform.Translate(translateY);

                // Move the orbitVector with the same values, along the camera's axes. In effect causing it to behave as if temporarily parented.
                orbitVector.transform.Translate(translateX, transform);
                orbitVector.transform.Translate(translateY, transform);
            }
            // If we're not currently navigating, grab selection if something is clicked
        }
        else if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            bool allowMultiSelect = false;

            // See if the user is holding in CTRL or SHIFT. If so, enable multiselection
            if (Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl))
            {
                allowMultiSelect = true;
            }

            // Something was clicked. Fetch.
            if (Physics.Raycast(ray, out hitInfo, GetComponent<Camera>().farClipPlane))
            {
                Transform target = hitInfo.transform;

                // If NOT multiselection, remove all prior selections
                if (!allowMultiSelect)
                {
                    deselectAll();
                }

                //Toggle between selected and unselected (depending on current state)
 /*               if (target.GetComponent<Renderer>().sharedMaterial != materialForSelection)
                {
                    selectedObjects.Add(target.gameObject);
                    selectedObjectsMaterial.Add(target.gameObject.GetComponent<Renderer>().sharedMaterial);
                    target.gameObject.GetComponent<Renderer>().sharedMaterial = materialForSelection;

                }
                else
                {
                    int arrayLocation = selectedObjects.IndexOf(target.gameObject);
                    if (arrayLocation == -1) { return; }; //this shouldn't happen. Ever. But still.

                    target.gameObject.GetComponent<Renderer>().sharedMaterial = selectedObjectsMaterial[arrayLocation];
                    selectedObjects.RemoveAt(arrayLocation);
                    selectedObjectsMaterial.RemoveAt(arrayLocation);

                }*/

                // Else deselect all selected objects (ie. click on empty background)
            }
            else
            {

                // Don't deselect if allowMultiSelect is true
                if (!allowMultiSelect) { deselectAll(); };
            }



            // Fetch input of the F-button (focus) -- this is a very dodgy implementation...
        }
        else if (Input.GetKeyDown("f"))
        {
            var backtrack = new Vector3(0, 0, -15);
            GameObject selectedObject;

            // If dealing with only one selected object
            if (selectedObjects.Count == 1)
            {
                selectedObject = selectedObjects[0];
                transform.position = selectedObject.transform.position;
                orbitVector.transform.position = selectedObject.transform.position;
                transform.Translate(backtrack);

                // Else we need to average out the position vectors (this is the proper dodgy part of the implementation)
            }
            else if (selectedObjects.Count > 1)
            {
                selectedObject = selectedObjects[0];
                var average = selectedObject.transform.position;

                for (var i = 1; i < selectedObjects.Count; i++)
                {
                    selectedObject = selectedObjects[i];
                    average = (average + selectedObject.transform.position) / 2;
                }

                transform.position = average;
                orbitVector.transform.position = average;
                transform.Translate(backtrack);
            }
        }
    }

    // Function to handle the de-selection of all objects in scene
    public void deselectAll()
    {

        // Run through the list of selected objects and restore their original materials
        for (var currentItem = 0; currentItem < selectedObjects.Count; currentItem++)
        {
            GameObject selectedObject  = selectedObjects[currentItem];
            selectedObject.GetComponent< Renderer > ().sharedMaterial = selectedObjectsMaterial[currentItem];
        }

        // Clear both arrays
        selectedObjects.Clear();
        selectedObjectsMaterial.Clear();
    }
}