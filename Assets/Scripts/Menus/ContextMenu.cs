﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using  System.Collections.Generic;
using System.Linq;

public class ContextMenu : MonoBehaviour
{
    public static  ContextMenu Instance { get; private set; }

    public bool SetActive {
        set { gameObject.SetActive(value); }
    }

    void Start()
    {
        Instance = this as ContextMenu;
        _rayTracer = GameObject.FindObjectOfType<RayTracer>();
    }

    public RaycastHit CurrentHit { get; set; }
    private RayTracer _rayTracer;

    public GameObject Target { get; set; }
    private GameObject _target;

    public Vector3 HitPosition { get; set; }

    public void OnCreateWpButton()
    {
        if(_rayTracer.Gizmo==null)
            Debug.LogError("Raytracer Gizmo is null desu");

        _target = Instantiate(_rayTracer.Gizmo,
            new Vector3(HitPosition.x, HitPosition.y, HitPosition.z),
            Quaternion.identity) as GameObject;

        foreach (var m in FindObjectsOfType<Movement>().ToList())
        {
            _target.transform.name = "Gizmo_"+m.ID+"_"+GizmoContainer.Instance.Paths[m.ID].Count;
            _target.transform.parent = GameObject.Find("GIZMOS").transform;
            GizmoContainer.Instance.Paths[m.ID].Add(_target.transform);
        }

        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnDropWpButton()
    {
        if (_rayTracer.Gizmo == null)
            Debug.LogError("Raytracer Gizmo is null desu");

        if(CurrentHit.collider.GetComponent<WPGizmo>()==null)
            return;

        Destroy(CurrentHit.collider.gameObject);

        foreach (var m in FindObjectsOfType<Movement>().ToList())
        {
            GizmoContainer.Instance.Paths[m.ID].RemoveAll(x => x == null);
        }

        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnFallowWpButton()
    {
        foreach (var m in FindObjectsOfType<Movement>().ToList())
        {
            m.Stop = false;
        }

        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnStandButton()
    {
        foreach (var m in FindObjectsOfType<SoldierAnim>().ToList())
        {
            m.Stand = true;
        }

        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnRunnButton()
    {
        foreach (var m in FindObjectsOfType<SoldierAnim>().ToList())
        {
            m.Run = true;
        }
        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnGranateButton()
    {
        foreach (var m in FindObjectsOfType<SoldierAnim>().ToList())
        {
            m.ThrowGranate();
        }
        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnReloadButton()
    {
        foreach (var m in FindObjectsOfType<SoldierAnim>().ToList())
        {
            m.MakeReload();
        }
        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnSitButton()
    {
        foreach (var m in FindObjectsOfType<SoldierAnim>().ToList())
        {
            m.Sit = true;
        }
        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnShootButton()
    {
        foreach (var m in FindObjectsOfType<SoldierAnim>().ToList())
        {
            m.Shoot = true;
        }
        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnLayButton()
    {
        foreach (var m in FindObjectsOfType<SoldierAnim>().ToList())
        {
            m.Lay = true;
        }
        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnWalkButton()
    {
        foreach (var m in FindObjectsOfType<SoldierAnim>().ToList())
        {
            m.Run = false;
            m.Walk = true;
        }
        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }

    public void OnCloseButton()
    {
        gameObject.SetActive(false);
        RayTracer.Instance.enabled = true;
    }
}
