﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GizmoContainer : MonoBehaviour {
    public static GizmoContainer Instance { get; private set; }
    public Dictionary<string, Movement> PathFollowers;
    public Dictionary<string, List<Transform>> Paths;

    void Awake()
    {
        Instance = this as GizmoContainer;
        PathFollowers= new Dictionary<string, Movement>();
        Paths = new Dictionary<string, List<Transform>>();

        foreach (var m in FindObjectsOfType<Movement>().ToList())
        {
            PathFollowers.Add(m.ID,m);
            Paths.Add(m.ID, new List<Transform>());
        }
    }

    void Start()
    {
        Instance = this as GizmoContainer;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
