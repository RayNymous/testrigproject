﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Security;

public class Movement : MonoBehaviour
{

    public string ID="TEST_OBJ";
    public float Speed = 4f;
    private int _currentWp = 0;
    private Transform _targetWp;
    private SoldierAnim soldierAnim;
    public bool Stop { get; set; }

    void Awake()
    {
        Stop = true;
        soldierAnim = GetComponent<SoldierAnim>();
    }

    void Update()
    {
        if (Stop)
        {
            //GetComponent<SoldierAnim>().Stand = true;
            soldierAnim.Run = false;
            return;
        }

        if (_currentWp < GizmoContainer.Instance.Paths[ID].Count)
        {
            if (_targetWp == null)
            {
                _targetWp = GizmoContainer.Instance.Paths[ID][_currentWp];
            }
            Walk();
        }
    }

    //move towards waypoints
    private void Walk()
    {
        soldierAnim.Run = true;
        //rotate to waypoint
        transform.forward = Vector3.RotateTowards(transform.forward, _targetWp.position - transform.position, Speed*Time.deltaTime, 0.0f);

        //move to wp
        transform.position=Vector3.MoveTowards(transform.position, _targetWp.position, Speed * Time.deltaTime);

        if (transform.position == _targetWp.position)
        {
            _currentWp++;
            if (_currentWp >= GizmoContainer.Instance.Paths[ID].Count)
            {//Send it TO FUCKIN SERVER
                Stop = true; 
                _currentWp = 0;
            }
            _targetWp = GizmoContainer.Instance.Paths[ID][_currentWp];
        }
    }
}


